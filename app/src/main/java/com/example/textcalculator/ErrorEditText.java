package com.example.textcalculator;

import android.text.TextUtils;
import android.widget.EditText;

public class ErrorEditText {
    private EditText oneEditText;
    private EditText twoEditText;

    public ErrorEditText(EditText oneEditText, EditText twoEditText){
        this.oneEditText = oneEditText;
        this.twoEditText = twoEditText;
    }

    public boolean InputFieldIsEmpty(EditText editText){
        if(TextUtils.isEmpty(editText.getText())){
            editText.setError("поле пустое!");
            return true;
        }
        return false;
    }
}
