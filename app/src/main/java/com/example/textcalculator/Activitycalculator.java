package com.example.textcalculator;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Activitycalculator extends AppCompatActivity {
    TextView oneTextView;
    TextView twoTextView;
    TextView operation;
    TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        oneTextView =  findViewById(R.id.textView1);
        twoTextView =  findViewById(R.id.textView2);
        operation =  findViewById(R.id.operation);
        textViewResult =  findViewById(R.id.textViewResult);

        Intent intent = getIntent();
        String message = intent.getStringExtra("Message");
        String message1 = intent.getStringExtra("Message1");
        String operationView = intent.getStringExtra("Operation");
        String result = intent.getStringExtra("Result");
        oneTextView.setText(message);
        twoTextView.setText(message1);
        operation.setText(operationView);
        textViewResult.setText(result);
    }
    public void reset(View view){
        Intent back = new Intent(this,MainActivity.class);
        startActivity(back);
    }

}
