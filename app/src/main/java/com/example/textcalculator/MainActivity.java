package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText oneEditText;
    private EditText twoEditText;
    private int firstNumber;
    private int secondNumber;
    private int valueResult;
    private String valueRes;
    private final String TAG = "Message";
    private final String TAG2 = "Message1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        oneEditText = findViewById(R.id.first_number);
        twoEditText = findViewById(R.id.second_number);
    }


    private void parseIntValues(){
        firstNumber = Integer.parseInt(oneEditText.getText().toString());
        secondNumber = Integer.parseInt(twoEditText.getText().toString());

    }
    public void addition(View view){
        if (errorInputField()) return;
        parseIntValues();
        valueResult = firstNumber + secondNumber;
        String operation = "+";
        newActivityWithSolution(operation);
    }

    public void subtraction(View view){
        if (errorInputField()) return;
        parseIntValues();
        valueResult = firstNumber - secondNumber;
        String operation = "-";
        newActivityWithSolution(operation);
    }
    public void multiplication(View view){
        if (errorInputField()) return;
        parseIntValues();
        valueResult = firstNumber * secondNumber;
        String operation = "*";
        newActivityWithSolution(operation);
    }
    public void delete(View view){
        if (errorInputField()) return;
        parseIntValues();
        valueResult = firstNumber / secondNumber;
        String operation = "/";
        newActivityWithSolution(operation);
    }
    private void  newActivityWithSolution(String operation){
        Intent intent = new Intent(this,Activitycalculator.class);
        valueRes = String.valueOf(valueResult);
        intent.putExtra(TAG, oneEditText.getText().toString());
        intent.putExtra(TAG2, twoEditText.getText().toString());
        intent.putExtra("Operation", operation);
        intent.putExtra("Result", valueRes);
        startActivity(intent);
    }
    private boolean errorInputField() {
        ErrorEditText errorEditText = new ErrorEditText(oneEditText, twoEditText);
        if(errorEditText.InputFieldIsEmpty(oneEditText) | errorEditText.InputFieldIsEmpty(twoEditText)){
            return true;
        }
        return false;
    }
}
